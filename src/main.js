import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueSocketIO from 'vue-socket.io'

Vue.config.productionTip = false

Vue.use(new VueSocketIO({
  // debug: true,
  connection: 'https://localhost:8080',
  vuex: {
    store,
    actionPrefix: 'SOCKET_',
    mutationPrefix: 'SOCKET_'
  },
  // options: { path: "/room/new" }
}))

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
}).$mount('#app')
