import Vue from 'vue';
import Vuex from 'vuex';
import map from './map';
import player from './player';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    map,
    player,
  },
  state: {
    isConnected: false,
    socketMessage: '',
  },
  mutations: {
    SOCKET_connect(state) {
      state.isConnected = true;
    },
    SOCKET_disconnect(state) {
      state.isConnected = false;
    },
    SOCKET_messagechannel(state, message) {
      state.socketMessage = message;
    },
  },
});
