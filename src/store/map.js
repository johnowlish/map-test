import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const map = {
  state: {
    map: null,
  },
  actions: {
    setMap({ state }, map) {
      state.map = map;
    },
  }
};

export default map;
