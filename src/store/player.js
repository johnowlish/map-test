import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const player = {
  state: {
    player: {},
  },
  getters: {
    getPlayer: state => state.player,
  },
  actions: {
    setPlayer({ commit }, player) {
      commit('setPlayer', player);
    },
    setPlayersPos({ commit }, position) {
      commit('setPlayersPos', position);
    },
    setPlayersImage({ state }, image) {
      state.player.image = image;
    },
    movePlayer({ commit }, step) {
      commit('movePlayer', step);
    },
  },
  mutations: {
    setPlayer(state, player) {
      // state.player = JSON.parse(player);
      state.player = player;
    },
    setPlayersPos(state, position) {
      state.player.position = position;
    },
    movePlayer(state, step) {
      state.player.position.x += step.x;
      state.player.position.y += step.y;
    },
  },
};

export default player;
