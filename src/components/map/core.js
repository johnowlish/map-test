import * as PIXI from 'pixi.js';
// import * as gsap from 'gsap';
import {
  mapState,
  mapActions,
} from 'vuex';
import images from './images';


const cellTextStyle = new PIXI.TextStyle({
  fontFamily: 'Arial',
  fontSize: 12,
  fontWeight: 'bold',
  fill: ['#3B3B51'],
});

export default {
  name: 'Map',
  data() {
    return {
      cellWidth: 0,
      cellHeight: 0,
      app: null,
      oldMap: [],
      players: [],
      oldPlayers: [],
    };
  },
  watch: {
    getMap(val) {
      if (this.oldMap.length > 0) {
        this.updateMap(val);
      } else {
        this.showMap(val);
      }
    }
  },
  computed: {
    ...mapState({
			getMap: state => state.map.map,
			getPlayer: state => state.player.player,
    })
  },
  methods: {
    ...mapActions([
      'setPlayersPos',
      'setPlayersImage',
    ]),
    initPixi() {
      this.app = new PIXI.Application({
        transparent: true
      });
      this.app.view.style.width = '100%';
      this.app.view.style.height = '100%';
      this.$el.appendChild(this.app.view);
      this.app.renderer.autoDensity = true;
      this.app.renderer.resize(900, 900);
    },
    updateMap(map){
      this.getPlayers(map, this.players);
      this.diffPlayers();
      this.oldMap = map;
      this.oldPlayers = this.players;
      this.players = [];
    },
    getPlayers (map, players) {
      for (let i = 0; i < map.length; i++) {
        for (let j = 0; j < map[i].length; j++)
        if (map[i][j] !== null) {
          players.push({
            id: map[i][j],
            position: {
              x: i,
              y: j
            },
          });
        }
      }
    },
    diffPlayers () {
      this.players.forEach(player => {
        let tempPlayer = this.oldPlayers.find(({ id }) => {
          return id === player.id;
        });
        if (tempPlayer) {
          this.movePlayer(player, tempPlayer);
        }
      });
    },
    movePlayer (oldPlayer, newPlayer) {
      let deltaX = oldPlayer.position.x - newPlayer.position.x;
      let deltaY = oldPlayer.position.y - newPlayer.position.y;
      let player = this.app.stage.children[0].getChildByName('player_player' + newPlayer.id);
      if (deltaX !== 0) {
        player.position.x += this.cellWidth * deltaX;
      } else if (deltaY !== 0) {
        player.position.y += this.cellHeight * deltaY;
      } else {
        console.log('dont move');
      }
    },
    showMap(map) {
      const mapContainer = new PIXI.Container();
      mapContainer.name = 'map_container';
      this.cellWidth = this.$refs.map.clientWidth / (map[0].length - 1);
      this.cellHeight = this.$refs.map.clientHeight / (map.length + 1);

      this.getPlayers(map, this.players);

      for (let row = 0; row < map.length; row++) {
        for (let col = 0; col < map[row].length; col++) {
          let cell = this.createCell(row, col, false);
          mapContainer.addChild(cell);
          if (map[row][col] !== null) {
            let player = this.createPlayer(map[row][col], row, col);
            mapContainer.addChild(player);
          }
        }
        this.oldMap = map;
      }
      this.app.stage.addChild(mapContainer);
    },
    createCell(row, col, player) {
      const cell = new PIXI.Graphics();
      cell.name = `cell_${row}_${col}`;
      cell.beginFill(0xFFFFFF, 1);
      player ? cell.lineStyle(5, 0xFF0000) : cell.lineStyle(0, 0x0000FF);
      cell.lineStyle(0, 0x0000FF);
      cell.drawRect(
        0,
        0,
        this.cellWidth,
        this.cellHeight,
      );
      cell.endFill();
      return cell;
    },
    createPlayer(id, row, col) {
      const playerImage = id === 0 ? 'drum_andrey' : 'drum_enemy';
      // const playerImage = this.getPlayer.id == id ? 'drum_andrey' : 'drum_enemy';
      const texture = PIXI.Texture.from(images[playerImage]);
      const player = new PIXI.Sprite(texture);
      player.name = `player_player${id}`;
      player.width = this.cellWidth - 5;
      player.height = this.cellHeight - 5;
      player.position.set(
        row * this.cellWidth + 15,
        col * this.cellHeight + 10,
      );
      return player;
    },
  },
  mounted() {
    this.initPixi();
    if (this.getMap) {
      this.showMap(this.getMap);
    }
  },
};
