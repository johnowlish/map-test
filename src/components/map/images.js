import drum_andrey_texture from '@/assets/images/drum_andrey.png';
import drum_enemy_texture from '@/assets/images/drum_enemy.png';

export default {
  drum_andrey: drum_andrey_texture,
  drum_enemy: drum_enemy_texture,
};
