import {
  mapGetters,
  mapActions,
} from 'vuex';

export default {
  name: 'Controls',
  computed: {
    ...mapGetters([
      'getPlayer',
    ])
  },
  methods: {
    ...mapActions([
      'movePlayer',
    ]),
    move(data) {
      this.$socket.emit('turn', {
        room_id: 0,
        player_id: this.getPlayer.id,
        direction: data,
      });
    },
  },
};
